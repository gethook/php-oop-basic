<?php 
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';
$sheep = new Animal("shaun");
echo $sheep->name;
echo "<br>";
echo $sheep->legs;
echo "<br>";
echo $sheep->cold_blooded;
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
?>