<?php 
require_once 'Animal.php';
/**
 * 
 */
class Frog extends Animal
{
	protected $legs = 4;
	
	function __construct($name)
	{
		parent::__construct($name);
	}

	public function jump()
	{
		echo "hop hop <br>";
	}
}
 ?>