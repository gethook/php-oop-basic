<?php 
/**
 * 
 */
class Animal
{
	protected $name;
	protected $legs = 2;
	protected $cold_blooded = "false";
	
	function __construct($name='')
	{
		$this->name = $name;
	}

	public function __get($prop)
	{
		if (property_exists($this, $prop)) {
			return $this->$prop;
		}
	}
}
 
?>